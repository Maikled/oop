﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			if (radius <= 0)
				throw new ArgumentOutOfRangeException(nameof(radius), "Invalid argument");

			Circle shape = new Circle();
			shape.Area = Math.PI * Math.Pow(radius, 2);
			shape.Perimeter = 2 * Math.PI * radius;
			shape.VertexCount = 0;
			return shape;
		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			if (a <= 0 || b <= 0 || c <= 0)
				throw new ArgumentOutOfRangeException(String.Join(nameof(a), nameof(b), nameof(c)), "Invalid arguments");

			if (a + b < c || a + c < b || b + c < a)
				throw new InvalidOperationException("Invalid arguments");

			Triangle triangle = new Triangle();
			triangle.Perimeter = a + b + c;
			var halfPerimetr = triangle.Perimeter / 2;
			triangle.Area = Math.Sqrt(halfPerimetr * (halfPerimetr - a) * (halfPerimetr - b) * (halfPerimetr - c));
			triangle.VertexCount = 3;
			return triangle;
		}

		public Shape CreateSquare(int a)
		{
			if (a <= 0)
				throw new ArgumentOutOfRangeException(nameof(a), "Invalid argument");

			Square square = new Square();
			square.VertexCount = 4;
			square.Perimeter = a * square.VertexCount;
			square.Area = a * a;
			return square;
		}

		public Shape CreateRectangle(int a, int b)
		{
			if (a <= 0 || b <= 0)
				throw new ArgumentOutOfRangeException(String.Join(nameof(a), nameof(b)), "Invalid arguments");

			Rectangle rectangle = new Rectangle();
			rectangle.VertexCount = 4;
			rectangle.Perimeter = a * 2 + b * 2;
			rectangle.Area = a * b;
			return rectangle;
		}
	}
}