﻿namespace Oop.Shapes
{
	public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public double Area { get; set; }

		/// <summary>
		/// Периметр
		/// </summary>
		public double Perimeter { get; set; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public int VertexCount { get; set; }

		public virtual bool IsEqual(Shape shape)
        {
			return false;
        }
	}

	public class Circle : Shape
    {
        public override bool IsEqual(Shape shape)
        {
			Circle circle = shape as Circle;
			if (circle == null) 
				return false;

			return shape.Area == this.Area;
        }
    }

	public class Triangle : Shape
    {
        public override bool IsEqual(Shape shape)
        {
            Triangle triangle = shape as Triangle;
			if (triangle == null)
				return false;
			
			return triangle.Area == this.Area;
        }
    }

	public class Square : Shape
    {
        public override bool IsEqual(Shape shape)
        {
			Square square = shape as Square;
			if (square == null)
				return false;
			
			return square.Area == this.Area;
        }
    }

	public class Rectangle : Shape
    {
		public override bool IsEqual(Shape shape)
		{
			Rectangle rectangle = shape as Rectangle;
			if (rectangle == null)
				return false;

			return rectangle.Area == this.Area;
		}
    }
}
